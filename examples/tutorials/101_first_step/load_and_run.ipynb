{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Aidge demonstration\n",
    "\n",
    "Aidge is a collaborative open source deep learning library optimized for export and processing on embedded devices. With Aidge, you can create or import a Computational Graph from common Frameworks, apply editing on its structure, train it and export its architecture on many embedded devices. Aidge provides optimized functions for inference as well as training and many custom functionalities for the target device.\n",
    "\n",
    "This notebook put in perspective the tool chain to import a Deep Neural Network from ONNX model and support its Inference in Aidge. The tool chain demonstrated is : \n",
    "\n",
    "![pipeline(1)](./static/pipeline_1.png)\n",
    " \n",
    "In order to demonstrate this toolchain, the MNIST digit recognition task is used.\n",
    "\n",
    "![MNIST](./static/MnistExamples.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Setting up the notebook"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### (if needed) Download the model\n",
    "\n",
    "If you don't have git-lfs, you can download the model and data using this piece of code"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import requests\n",
    "\n",
    "def download_material(path: str) -> None:\n",
    "    if not os.path.isfile(path):\n",
    "        response = requests.get(\"https://gitlab.eclipse.org/eclipse/aidge/aidge/-/raw/dev/examples/tutorials/101_first_step/\"+path+\"?ref_type=heads\")\n",
    "        if response.status_code == 200:\n",
    "            with open(path, 'wb') as f:\n",
    "                f.write(response.content)\n",
    "            print(\"File downloaded successfully.\")\n",
    "        else:\n",
    "            print(\"Failed to download file. Status code:\", response.status_code)\n",
    "\n",
    "# Download onnx model file\n",
    "download_material(\"MLP_MNIST.onnx\")\n",
    "# Download input data\n",
    "download_material(\"input_digit.npy\")\n",
    "# Download output data for later comparison\n",
    "download_material(\"output_digit.npy\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define mermaid visualizer function\n",
    "\n",
    "Aidge save graph using the mermaid format, in order to visualize the graph live in the notebook, we will setup the following function: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import base64\n",
    "from IPython.display import Image, display\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "def visualize_mmd(path_to_mmd):\n",
    "  with open(path_to_mmd, \"r\") as file_mmd:\n",
    "    graph_mmd = file_mmd.read()\n",
    "\n",
    "  graphbytes = graph_mmd.encode(\"utf-8\")\n",
    "  base64_bytes = base64.b64encode(graphbytes)\n",
    "  base64_string = base64_bytes.decode(\"utf-8\")\n",
    "  display(Image(url=f\"https://mermaid.ink/img/{base64_string}\"))\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import Aidge\n",
    "\n",
    "In order to provide a colaborative environnement in the plateform, the structure of Aidge is built on a core library that interfaces with multiple modules binded to python libraries. \n",
    "- ``aidge_core`` is the core library and offers all the basic functionnalities to create and manipulate the internal graph representation\n",
    "- ``aidge_backend_cpu`` is a C++ module providing a generic C++ implementations for each component of the graph\n",
    "- ``aidge_onnx`` is a module allowing to import ONNX to the Aidge framework\n",
    "- ``aidge_export_cpp`` is a module dedicated to the generation of optimized C++ code\n",
    "\n",
    "This way, ``aidge_core`` is free of any dependencies and the user can install what he wants depending on his use case."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import aidge_core\n",
    "\n",
    "# Conv2D Operator is available but no implementation has been loaded\n",
    "print(f\"Available backends:\\n{aidge_core.get_keys_ConvOp2D()}\")\n",
    "\n",
    "# note: Tensor is a special case as 'cpu' backend is provided in the core\n",
    "# module to guarantee basic functionalities such as data accesss\n",
    "print(f\"Available backends for Tensor:\\n{aidge_core.Tensor.get_available_backends()}\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, no backends are availables for the class ``Conv2D``.\n",
    "\n",
    "We need to import the ``aidge_backend_cpu`` module which will register itself automatically to ``aidge_core``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import aidge_backend_cpu\n",
    "\n",
    "print(f\"Available backends:\\n{aidge_core.get_keys_ConvOp2D()}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For this tutorial, we will need to import ``aidge_onnx`` in order to load ONNX files, numpy in order to load data and matplotlib to display images."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "import aidge_onnx\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ONNX Import\n",
    "Import an ONNX model into Aidge internal graph representation.\n",
    "\n",
    "![pipeline(2)](./static/pipeline_2.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = aidge_onnx.load_onnx(\"MLP_MNIST.onnx\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see in the logs, aidge imported a Node as a ``GenericOperator``:\n",
    "\n",
    "```\n",
    "- /Flatten_output_0 (Flatten | GenericOperator)\n",
    "```\n",
    "\n",
    "This is a fallback mechanism which allow aidge to load ONNX graph without failing even when encountering a node which is not available.\n",
    "The ``GenericOperator`` act as a stub retrieving node type and attributes from ONNX. This allow to provide an implementation in a user script or as we will see to remove/replace them using aidge recipes. \n",
    "\n",
    "You can visualize the graph using the ``save`` method and the mermaid visualizer we have setup."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.save(\"myModel\")\n",
    "visualize_mmd(\"myModel.mmd\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Graph transformation \n",
    "\n",
    "![pipeline(3)](./static/pipeline_3.png)\n",
    "\n",
    "In order to support the graph for inference we need to support all operators.\n",
    "The imported model contains ```Flatten``` before the ```Gemm```  operator. The ```aidge.FC``` operator already supports the flatten operation. \n",
    "Graph transformation is required to support the graph for inference, i.e. remove the ```Flatten``` operator. \n",
    "\n",
    "Aidge graph transformation toolchain is the following process :\n",
    "\n",
    "**1. Describe the graph pattern**\n",
    "\n",
    "\n",
    "In order to find specific patterns inside a graph, there is first a need to describe those patterns. Aidge introduces an innovative way to describe graph patterns, **Graph Regular Expression**, inspired by regular expression from the formal language theory.\n",
    "\n",
    "In this example the GraphRegEx used would be simple:\n",
    "\n",
    "```\n",
    "\"Flatten->FC;\"\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "graph_regex = aidge_core.GraphRegex()\n",
    "graph_regex.set_node_key(\"Flatten\", \"getType($) =='Flatten'\")\n",
    "graph_regex.set_node_key(\"FC\", \"getType($) =='FC'\")\n",
    "graph_regex.add_query(\"Flatten -> FC\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "**2. Match the described pattern**\n",
    "\n",
    "Once the graph pattern is described with a graph regular expression, we apply an innovative graph matching algorithm to find patterns corresponding to the description.\n",
    "\n",
    "This alogrithm will return all the matched patterns described with a graph regular expression in a [match](https://eclipse-aidge.readthedocs.io/en/latest/source/API/Core/graphMatching.html#match) class. One matched pattern is the combinaison of the graph pattern start nodes and all the nodes in the matched pattern (including the start nodes).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "all_match = graph_regex.match(model)\n",
    "print('Number of match : ', len(all_match))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, we have one match :\n",
    "- List of one list containing the start node : [[Flatten node]]\n",
    "- List of one set containing all the matched nodes : [{Flatten node, FC node}]\n",
    "\n",
    "Let's visualize the match :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('The start node : ')\n",
    "for match in all_match:\n",
    "    print('\\t', match.get_start_node()[0].type())\n",
    "    print('All the matched nodes for', match.get_query() , ':')\n",
    "    for n in match.get_all():\n",
    "        print('\\t', n.type())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**3. Apply graph transformations on the matched patterns**\n",
    "\n",
    "Now that we have matched the desired patterns we can apply graph transformation on it. The main graph transformation functions (currently under dev) are : \n",
    "- Replace the current GraphView with a set of given Nodes if possible : [replace](https://eclipse-aidge.readthedocs.io/en/latest/source/API/Core/graph.html#aidge_core.GraphView.replace)\n",
    "- Insert a node (newParentNode) as a parent of the passed node (childNode) : [insert_parent](https://eclipse-aidge.readthedocs.io/en/latest/source/API/Core/graph.html#_CPPv4N5Aidge9GraphView12insertParentE7NodePtr7NodePtr9IOIndex_t9IOIndex_t9IOIndex_t)\n",
    "- Remove a node : remove() \n",
    "\n",
    "In this example we remove the ```Flatten``` operator from the graph using replace.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "g = aidge_core.GraphView()\n",
    "g.add(next(iter(all_match)).get_start_node()[0])\n",
    "aidge_core.GraphView.replace(g.get_nodes(), set())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The flatten is removed, let's visualize the model :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.save(\"mySupportedModel\")\n",
    "visualize_mmd(\"mySupportedModel.mmd\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "All of these steps are embedded inside ``recipes`` functions. These recipes are available in ``aidge_core``, some recipes are:\n",
    "- *fuse_batchnorm*: Fuse BatchNorm inside Conv or FC operator;\n",
    "- *fuse_mul_add*: Fuse MatMul and Add operator into a FC operator;\n",
    "- *remove_flatten*: Remove Flatten if it is before an FC operator.\n",
    "\n",
    "Let's do it again with the *remove_flatten* recipie :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import model again\n",
    "model = aidge_onnx.load_onnx(\"MLP_MNIST.onnx\")\n",
    "# Use remove_flatten recipie\n",
    "aidge_core.remove_flatten(model)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This time the flatten is removed with the recipie, let's visualize the model :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.save(\"mySupportedModel\")\n",
    "visualize_mmd(\"mySupportedModel.mmd\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inference\n",
    "\n",
    "We now have a graph fully supported by aidge, we are ready to do some inference !\n",
    "\n",
    "![pipeline(4)](./static/pipeline_4.png)\n",
    "\n",
    "### Create an input tensor & its node in the graph\n",
    "\n",
    "In order to perform an inferencewe will load an image from the MNIST dataset using Numpy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Load input data & its output from the MNIST_model\n",
    "digit = np.load(\"input_digit.npy\")\n",
    "plt.imshow(digit[0][0], cmap='gray')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And in order to validate the result our model will provide, we will also load the output the PyTorch model povided for this image"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "output_model = np.load(\"output_digit.npy\")\n",
    "print(output_model)\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thanks to the Numpy interoperability we can create an Aidge ``Tensor`` using directly the numpy array storing the image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "input_tensor = aidge_core.Tensor(digit)\n",
    "print(f\"Aidge Input Tensor dimensions: \\n{input_tensor.dims()}\")\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To add an input to the graph we can create a ``Producer`` node, insert it in the ``GraphView`` and set its output with the ``Tensor`` we have just created, or data can simply be fed to the ``GraphView`` via the ``scheduler`` ```forward()``` call."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Configure the model for inference"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At the moment the model has no implementation, it is only a datastructure. To set an implementation we will set a dataype and a backend."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Configure the model\n",
    "model.compile(\"cpu\", aidge_core.dtype.float32, dims=[[1,1,28,28]])\n",
    "# equivalent to set_datatype(), set_backend() and forward_dims()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Create a scheduler and run inference\n",
    "\n",
    "The graph is ready to run ! We just need to schedule the execution, to do this we will create a ``Scheduler`` object, which will take the graph and generate an optimized scheduling using a consummer producer heuristic.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# Create SCHEDULER\n",
    "scheduler = aidge_core.SequentialScheduler(model)\n",
    "\n",
    "# Run inference !\n",
    "scheduler.forward(data=[input_tensor])\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Assert results\n",
    "for outNode in model.get_output_nodes():\n",
    "    output_aidge = np.array(outNode.get_operator().get_output(0))\n",
    "    print(output_aidge)\n",
    "    print('Aidge prediction = ', np.argmax(output_aidge[0]))\n",
    "    assert(np.allclose(output_aidge, output_model,rtol=1e-04))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible to save the scheduling in a mermaid format using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "scheduler.save_scheduling_diagram(\"schedulingSequential\")\n",
    "visualize_mmd(\"schedulingSequential.mmd\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Export\n",
    "\n",
    "Now that we have tested the imported graph we can look at one of the main feature of Aidge, the export of computationnal graph to an hardware target using code generation.\n",
    "\n",
    "![pipeline(5)](./static/pipeline_5.png)\n",
    "\n",
    "### Generate an export in C++\n",
    "In this example we will generate a generic C++ export.\n",
    "This export is not based on the `cpu` backend we have set before.\n",
    "\n",
    "In this example we will create a standalone export which is abstracted from the Aidge platform.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "! rm -r myexport\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls myexport\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Generating a ``cpu`` export recquires the ``aidge_export_cpp`` module.\n",
    "\n",
    "Once the module is imported you just need one line to generate an export of the graph."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import aidge_export_cpp\n",
    "# Freeze the model by setting constant to parameters producers\n",
    "for node in model.get_nodes():\n",
    "    if node.type() == \"Producer\":\n",
    "        node.get_operator().attr.constant = True\n",
    "\n",
    "# Create Producer Node for the Graph\n",
    "input_node = aidge_core.Producer([1, 1, 28, 28], \"input\")\n",
    "input_node.add_child(model)\n",
    "model.add(input_node)\n",
    "\n",
    "# Configuration for the model + forward dimensions\n",
    "model.compile(\"cpu\", aidge_core.dtype.float32)\n",
    "# Export the model in C++ standalone\n",
    "aidge_export_cpp.export(\"myexport\", model, scheduler)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The export function will generate :\n",
    "- **dnn/layers** layers configuration;\n",
    "- **dnn/parameters** folder with parameters;\n",
    "- **dnn/include/dnn.h** API to use the export;\n",
    "- **dnn/include/network_functions.h** header file for kernels;\n",
    "- **dnn/memory** memory management information;\n",
    "- **dnn/src** kernel source code + forward function;\n",
    "- **main.cpp** This file is an export of the scheduler, it allows\n",
    "- **Makefile** To compile the main.cpp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!tree myexport\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generate an input file for tests\n",
    "\n",
    "To test the export we need to provide data, to do so we will export the numpy array using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "aidge_export_cpp.generate_input_file(array_name=\"inputs\", array=digit.reshape(-1), export_folder=\"myexport\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Compile the export"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cd myexport && make\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Run the export"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!./myexport/bin/run_export\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "env",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.14"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
