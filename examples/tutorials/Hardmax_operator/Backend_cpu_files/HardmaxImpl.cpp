/********************************************************************************
 * Copyright (c) 2024 CEA-List
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 ********************************************************************************/

#include <cassert>
#include <chrono>  // std::chrono::milliseconds
#include <numeric> // std::accumulate
#include <thread>  // std::this_thread::sleep_for
#include <vector>

#include "aidge/operator/Hardmax.hpp"
#include "aidge/utils/Types.h"

#include "aidge/backend/cpu/operator/HardmaxImpl.hpp"
#include "aidge/backend/cpu/operator/HardmaxImpl_forward_kernels.hpp"

void Aidge::HardmaxImpl_cpu::forward() {
    const Hardmax_Op& op_ = dynamic_cast<const Hardmax_Op&>(mOp);

    // Check if input is provided
    assert(op_.getInput(0) && "missing input");

    // Create the forward kernal with the wanted types
    auto kernelFunc = Registrar<HardmaxImplForward_cpu>::create({
        op_.getInput(0)->dataType(),
        op_.getOutput(0)->dataType()});

    // Call kernel
    kernelFunc(dynamic_cast<const Hardmax_Op&>(mOp).getStaticAttributes(),
        op_.getInput(0)->dims(),
        op_.getInput(0)->getImpl()->rawPtr(),
        op_.getOutput(0)->getImpl()->rawPtr());
}
