Tutorials
=========

To get started with Aidge, please follow the `Aidge 101 tutorial <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/dev/examples/tutorials/Aidge_tutorial/load_and_run.ipynb?ref_type=heads>`_.
This tutorial demonstrates the basic features of the Aidge Framework, importing an ONNX, transforming a neural network graph, performing inference and a cpp export.

For an advanced usage of the Aidge Framework, please refer to the following tutorials. 

Aidge DNN fonctionnalities
--------------------------

- `Manipulating databases and creating batches of data <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/dev/examples/tutorials/Database_DataProvider_tutorial/Database.ipynb?ref_type=heads>`_
- 🚧 Train a Deep Neural Network
- `Provide an operator implementation using Python or atomic operators <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/dev/examples/tutorials/Onnx_tutorial/onnx_tuto.ipynb?ref_type=heads>`_
- `Perform advanced graph matching with the Graph Regular Expression tool <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/dev/examples/tutorials/GraphRegex_tutorial/GraphRegex.ipynb?ref_type=heads>`_

Optimize your DNN with Aidge
----------------------------

- 🚧 Optimize your neural network with Post Training Quantization
- `Optimize the inference of your neural network with Tiling <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/dev/examples/tutorials/Tiling_tutorial/tiling.ipynb?ref_type=heads>`_

Export your DNN with Aidge
--------------------------

- `Add a custom implementation for a cpp export <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/dev/examples/tutorials/Export_tutorial/add_custom_operator.ipynb?ref_type=heads>`_
- `Export your DNN with TensorRT <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/dev/examples/tutorials/TRT_Quantization_tutorial/tuto.ipynb?ref_type=heads>`_
- 🚧 Export your DNN for an STM32

Contributing to Aidge 
---------------------

You will find all information for contributing to Aidge in the `wiki <https://gitlab.eclipse.org/groups/eclipse/aidge/-/wikis/Contributing>`_.
For example, you can help us extend our operator coverage by adding an operator and its implementation in the Aidge library. 
The `Add an operator and its implementation Tutorial <https://gitlab.eclipse.org/eclipse/aidge/aidge/-/blob/dev/examples/tutorials/Hardmax_operator/operator_adding.ipynb?ref_type=heads>`_ details the steps to follow.

If you encounter any difficulty with the Tutorials, please create an issue `here <https://gitlab.eclipse.org/groups/eclipse/aidge/-/issues>`_.

