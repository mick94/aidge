Graph Matching
==============

Aidge introduces a simple and efficient DSL for graph matching, sometimes called "graph regex". It is possible to write complex textual queries to find a quantified or unquantified set of nodes with specific types, attributes and/or relationships between them. This is particularly useful to implement sophisticated pattern-matching heuristics with no effort!

Here is an example of a query that you can do in Aidge:

.. code-block:: python

    graph_regex = aidge_core.GraphRegex()
    graph_regex.add_query("(Pad#?->Conv#|Deconv#->Pad#?)->ReLU#?->Add#1?->ReLU#?->MaxPool#?->ReLU#?")
    graph_regex.add_query(".->Add#1?")

    for match in graph_regex.match(model):
        aidge_core.GraphView.replace(match, MyCustomIPOperator())

You can define your own node test function as well:

.. code-block:: python

    def myNodeTestingFunc(node):
        ...
        return predicate

    graph_regex = aidge_core.GraphRegex()
    graph_regex.set_node_key("test", myNodeTestingFunc)
    graph_regex.add_query("Conv->test")


There are two classes to use in order to define a match using graph regex mechanics.
The first one, GraphRegex, is used to define queries and to perform the match. 
The second one, MatchSolution, is there to assist in the exploitation of the results.

GraphRegex
-----------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.GraphRegex
            :members:
            :inherited-members:

    .. tab-item:: C++
        
        .. doxygenclass:: Aidge::GraphRegex

Match Solution
--------------

.. tab-set::

    .. tab-item:: Python

        .. autoclass:: aidge_core.MatchSolution
            :members:
            :inherited-members:

    .. tab-item:: C++
        
        .. doxygenclass:: Aidge::MatchSolution
