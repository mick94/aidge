Aidge core API
==============


.. toctree::
    :maxdepth: 1

    data.rst
    graph.rst
    operator.rst
    scheduler.rst
    graphRegex.rst
    recipes.rst
