Sphinx!=7.2.2  # Bug with 7.2.2, see https://github.com/breathe-doc/breathe/issues/944
sphinxcontrib-bibtex
breathe
sphinx-pyreverse
sphinxcontrib-mermaid
sphinx-toolbox
sphinx_gallery
sphinx_design
sphinx_copybutton
pydata-sphinx-theme
nbsphinx
IPython
ipykernel
nbsphinx-link
sphinx-jinja
# For notebooks execution:
matplotlib