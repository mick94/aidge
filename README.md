# Aidge

[![License-badge](https://img.shields.io/badge/License-EPL%202.0-blue.svg)](LICENSE) [![Documentation Status](https://readthedocs.org/projects/eclipse-aidge/badge/?version=latest)](https://eclipse-aidge.readthedocs.io/en/latest/?badge=latest)

The Eclipse Aidge platform is a comprehensive solution for fast and accurate Deep Neural Network (DNN) simulation and full and automated DNN-based application building. The platform integrates database construction, data pre-processing, network building, benchmarking and hardware export to various targets. It is particularly useful for DNN design and exploration, allowing simple and fast prototyping of DNN with different topologies. It is possible to define and learn multiple network topology variations and compare the performances (in terms of recognition rate and computational cost) automatically. Export hardware targets include CPU, DSP and GPU with OpenMP, OpenCL, Cuda, cuDNN and TensorRT programming models as well as custom hardware IP code generation with High-Level Synthesis for FPGA and dedicated configurable DNN accelerator IP.



| Module    | Status | Coverage |
| -------- | ------- | ------- |
| [aidge_core](https://gitlab.eclipse.org/eclipse/aidge/aidge_core)  | ![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/pipeline.svg?ignore_skipped=true) | ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_core/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)    |
| [aidge_backend_cpu](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu) | ![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/pipeline.svg?ignore_skipped=true) | ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cpu/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)     |
| [aidge_backend_cuda](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda) | ![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/pipeline.svg?ignore_skipped=true) | ![C++ coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/coverage.svg?job=coverage:ubuntu_cpp&key_text=C%2B%2B+coverage&key_width=90) ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_backend_cuda/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)     |
| [aidge_export_cpp](https://gitlab.eclipse.org/eclipse/aidge/aidge_export_cpp) |  |      |
| [aidge_onnx](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx)    | ![Pipeline status](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/badges/main/pipeline.svg?ignore_skipped=true) | ![Python coverage](https://gitlab.eclipse.org/eclipse/aidge/aidge_onnx/badges/main/coverage.svg?job=coverage:ubuntu_python&key_text=Python+coverage&key_width=100)    |


## System requirements

- ``CMake >= 3.15``
- ``Python >= 3.7``

Each Aidge package may add other requirements, please check them.

## Installation

This repository is a bundle containing Aidge modules. It will install the following modules
* `aidge_core`
* `aidge_backend_cpu`
* `aidge_backend_cuda`
* `aidge_backend_opencv`
* `aidge_export_cpp`
* `aidge_learning`
* `aidge_onnx`
* `aidge_quantize`

Feel free to install other modules if you need them with this bundle. <br>

### From Source

#### Build on Linux

This repository has several [submodules](https://git-scm.com/book/en/v2/Git-Tools-Submodules).
For this reason when cloning this repository you need to use the ``--recursive`` option.

```bash
git clone --recursive https://gitlab.eclipse.org/eclipse/aidge/aidge.git
```

Then we recommend creating a Python environment to work with Aidge. You can use virtualenv for example.
``` bash
virtualenv -p python3.8 env_aidge
source env_aidge/bin/activate
```
Install Aidge in your Python environment
``` bash
pip install .
```

You can test your installation by running
```bash
python -c "import aidge_core; import aidge_backend_cpu; print(aidge_core.Tensor.get_available_backends())"
```

You should have the following in your terminal:
```
{'cpu'}
```

## Docker Image

Feel free to use one of the Dockerfiles available in the [`docker`](docker) folder.

To build the image, run where your DockerFile is
```
docker build --pull --rm -f "path/to/dockerfile.Dockerfile" -t aidge:myenv .
```

Then to run a container, run
```
docker run --name mycontainer -it aidge:myenv
```
#### Build on Windows

On your session,
1) Install Visual Studio Community. Get it from https://visualstudio.microsoft.com/fr/vs/community/)
2) Install MSVC
3) Install GIT (Source code control program). Get it from: https://git-scm.com/download)
4) Install CMake (Solution for managing the software build process). Get it from: https://cmake.org/download/
5) Install Python (Programming language). Get it from: https://www.python.org/download/
6) Install pip
7) Install VS Code. Get it from https://code.visualstudio.com/download => add following extensions (Python, CMake)
8) create and launch virtual environnement (venv)
```
python -m venv myenv
```
```
.\myenv\Scripts\activate
```
9) Place in aidge repository and execute
```
python.exe .\setup.py install
```
You can test your installation by running
```bash
python -c "import aidge_core; import aidge_backend_cpu; print(aidge_core.Tensor.get_available_backends())"
```

You should have the following in your terminal:
```
{'cpu'}
```
## Contributing

If you would like to contribute to the Aidge project, we’re happy to have your help!
Everyone is welcome to contribute code via merge requests, to file issues on Gitlab,
to help people asking for help, fix bugs that people have filed,
to add to our documentation, or to help out in any other way.
We grant commit access (which includes full rights to the issue database, such as being able to edit labels)
to people who have gained our trust and demonstrated a commitment to Aidge.

## License

Aidge has an Eclipse Public License 2.0, as found in the [LICENSE](LICENSE).
