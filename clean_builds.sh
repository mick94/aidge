#!/bin/bash

# remove install directory
installdir=$(find . -type d -name "install")
echo "clean $installdir"
rm -rf ${installdir}

# remove build directories
BUILDPATHS=()

## Use the find command to search for directories named "build"
## in the current directory and its subdirectories
while IFS= read -r -d '' dir; do
  # Add each directory to the BUILDPATHS list
  BUILDPATHS+=("$dir")
done < <(find . -type d -name "build*" -print0)

## Print and remove the list of build paths
for path in "${BUILDPATHS[@]}"; do
  echo "clean $path"
  rm -rf ${path}
done

# remove egg-info
EGGINFOPATHS=()
while IFS= read -r -d '' dir; do
	EGGINFOPATHS+=("$dir")
done < <(find . -type d -name "*.egg-info" -print0)

for path in "${EGGINFOPATHS[@]}"; do
	echo "clean $path"
	rm -rf ${path}
done
