#!/bin/bash

find . -maxdepth 1 -type f -name "clean_*" ! -name "clean_all.sh" -exec {} \;
